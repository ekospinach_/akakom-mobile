package id.adc.akakommobile;

import id.adc.akakommobile.adapter.TabsProdiAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;
import android.support.v7.app.ActionBar.TabListener;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;

public class ProdiActivity extends ActionBarActivity implements TabListener {
	
	private ViewPager	     viewPager;
	private TabsProdiAdapter	mAdapter;
	private final String[]	 tabs	=
	                              { "Teknik\nInformatika", "Sistem\nInformasi", "Komputerisasi\nAkuntansi", "Manajemen\nInformatika",
	         "Teknik\nKomputer" };
	CharSequence title;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_prodi);
		
		title = getTitle();
		title = "Program Studi";

		viewPager = (ViewPager) findViewById(R.id.viewPagerProdi);
		mAdapter = new TabsProdiAdapter(getSupportFragmentManager());

		viewPager.setAdapter(mAdapter);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		// Adding Tabs
		for (String tab_name : tabs)
		{
			getSupportActionBar().addTab(
			        getSupportActionBar().newTab().setText(tab_name)
			                .setTabListener(this));
		}

		viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener()
		{

			@Override
			public void onPageSelected(int position)
			{
				// on changing the page
				// make respected tab selected
				getSupportActionBar().setSelectedNavigationItem(position);
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2)
			{
			}

			@Override
			public void onPageScrollStateChanged(int arg0)
			{
			}
		});
	}


	@Override
	public void onTabReselected(Tab arg0, FragmentTransaction arg1) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onTabSelected(Tab arg0,FragmentTransaction arg1) {
		// TODO Auto-generated method stub
		viewPager.setCurrentItem(arg0.getPosition());
	}


	@Override
	public void onTabUnselected(Tab arg0,FragmentTransaction arg1) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if(keyCode == KeyEvent.KEYCODE_BACK) {
			Intent main = new Intent(ProdiActivity.this, MainActivity.class);
			startActivity(main);
			this.finish();
		}
		return super.onKeyDown(keyCode, event);
	}

}
