package id.adc.akakommobile;

import id.adc.akakommobile.R;
import id.adc.akakommobile.lib.TextViewEx;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;

public class SplashActivity extends Activity {
	TextViewEx samb;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		
		getActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#3366ff")));
		samb = (TextViewEx) findViewById(R.id.textSambutan);
		samb.setText("Assalamualaikum wr.wb\n\n" +"Salam sejahtera untuk kita"
		                        + "\nSemua teman teman selamat datang di AKAKOM MOBILE, yang merupakan salah satu wadah yang di sponsori oleh mahasiswa � mahasiswa yang kreatif ini dapat menambah kreatifitas ke depan teman teman di komunitas ini. Harapan saya komunitas ini tidak hanya seperti jamur yang muncul saja "
		                        + "tetapi komunitas ini selalu berlanjut dan selalu berkembang. Bahkan STMIK AKAKOM selalu membantu dan mensupport apapun kegiatan IT yang sifatnya untuk  mengembangkan STMIK AKAKOM itu sendiri dan merubah watak karakter dari mahasiswa STMIK AKAKOM. "
		                        + "Harapan itu yang sangat saya harapkan, sehingga kesinambungan dari program program ini sangat di harapkan dan dapat bersinergis dengan bidang bidang yang lain tentunya. Demikian sambutan saya"
		                        + "\n\nWassalamualaikum wr.wb"
		                        + "\n\nSELAMAT BERKARYA!", true);
	}
	
	public void klikSkip(View v) {
		if(v.getId()==R.id.btnSkip) {
			Intent intent = new Intent(SplashActivity.this, MainActivity.class);
			startActivity(intent);
			finish();
		}
	}

}
