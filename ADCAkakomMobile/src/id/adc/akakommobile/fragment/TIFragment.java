package id.adc.akakommobile.fragment;

import id.adc.akakommobile.R;
import id.adc.akakommobile.lib.TextViewEx;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class TIFragment extends Fragment {
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_ti, container, false);
		
		TextViewEx pndTI = (TextViewEx) view.findViewById(R.id.textTI);
		TextViewEx visiTI = (TextViewEx) view.findViewById(R.id.textVisiTI);
		TextViewEx misiTI = (TextViewEx) view.findViewById(R.id.textMisiTI);
		TextViewEx tujuanTI = (TextViewEx) view.findViewById(R.id.textTujuanTI);
		pndTI.setText("Program Studi Teknik Informatika mulai diselenggarakan berdasarkan SK No. 300/DIKTI/Kep/1992. Saat ini Program Studi Teknik Informatika telah diakreditasi �B� berdasarkan SK No. 028/BAN-PT/Ak-IX/S1/I/2006.\n"
				+ "Minat untuk mengikuti pendidikan pada Program Studi Teknik Informatika sangat besar dan meningkat dari tahun ke tahun dan berasal dari semua propinsi di Indonesia. Hal ini dikarenakan kebutuhan alumni Program Studi Teknik Informatika   cukup tinggi karena hampir semua aspek membutuhkan teknologi informasi untuk berkembang.  Selain itu juga mengisyaratkan bahwa Program Studi Teknik Informatika  sudah dikenal dalam taraf nasional.\n "
				+ "Jumlah alumni Program Studi Teknik Informatika  hingga saat ini mencapai sekitar 1000 mahasiswa, yang telah bekerja pada perbankan, perminyakan, pendidikan, kesehatan , dan bidang-bidang lain. Banyak mahasiswa yang telah bekerja sebelum lulus dari pendidikannya. Hal ini membuktikan bahwa ilmu yang diterima mahasiwa bisa digunakan kapan saja. Beberapa lapangan kerja alumni diantaranya Krakatau Steel, BRI, Pertamina, TOTAL, (SGM), Instansi Negeri (Kejaksaaan, Pemda, PTN), dan Perguruan Tinggi Swasta.\n "
				+ "Kegiatan-kegiatan yang dilaksanakan oleh Program Studi Teknik Informatika  sangat beragam. Kegiatan tersebut dalam bentuk penelitian bersama dosen dan mahasiswa, partisipan / penyelenggara even  nasional maupun internasional  (seminar, workshop, kuliah umum), pelatihan teknologi informasi ke instansi/organisasi.", true);
		visiTI.setText("Jurusan Teknik Informatika sebagai pembentukan dan pengembangan sumber daya manusia yang mempunyai wawasan ICT (Information and Communication Technology) yang tinggi dalam bidang Rekayasa Perangkat Lunak (Mobile, Games & Jaringan) dan Sistem Cerdas/Komputasi", true);
		misiTI.setText("Mendidik Mahasiswa untuk siap produktif dan mampu mengembangan diri di dalam penguasaan ICT (Information and Communication Technology) khususnya pada bidang Rekayasa Perangkat Lunak (Mobile, Games & Jaringan) maupun bidang Sistem Cerdas/Komputasi, serta menerapakan dalam kehidupan bermasyarakat", true);
		tujuanTI.setText("Menghasilkan lulusan yang siap bekerja / produktif dalam dunia kerja baik secara tim maupun individu dalam penguasaan ICT (Information and Communication Technology) khususnya pada bidang Rekayasa Perangkat Lunak (Mobile, Games & Jaringan)) maupun bidang Sistem Cerdas/Komputasi dan dapat bekerjasama dalam tingkat lokal, nasional, regional maupun internasional", true);
		
		return view;
	}

}
