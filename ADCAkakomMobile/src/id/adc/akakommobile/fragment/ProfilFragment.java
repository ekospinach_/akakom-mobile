package id.adc.akakommobile.fragment;

import id.adc.akakommobile.R;
import id.adc.akakommobile.lib.TextViewEx;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class ProfilFragment extends Fragment{
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		View view = inflater.inflate(R.layout.fragment_profil, container, false);
		
		TextViewEx sej = (TextViewEx) view.findViewById(R.id.textSjrh);
		sej.setText(
		        "Yayasan Pendididkan Widya Bakti berdiri dengan Akta Notaris Nomor 43 tanggal 30 Juni 1979,"
		                + " sebagai penyelenggara pendidikan tinggi dibidang komputer yang pertama di Jateng dan DIY dengan nama Akademi Aplikasi Komputer (AKAKOM)."
		                + " Tahun 1985 AKAKOM menjadi AMIK AKAKOM dan akhirnya sejak tahun 1992 menjadi STMIK AKAKOM.",
		        true);

		TextViewEx vsms = (TextViewEx) view.findViewById(R.id.txtVsms);
		vsms.setText(
		        "VISI : STMIK AKAKOM mempunyai visi untuk menjadi perguruan tinggi unggulan"
		                + " bidang teknologi informasi yang bertumpu pada nilai nilai dan etika kehidupan yang baik,"
		                + " benar dan universal untuk mewujudkan penigkatan taraf hidup bangsa\n"
		                + "MISI : Adapun misinya adalah melakasanakan Tridharma perguruan  tinggi bidamg komputer dan "
		                + "teknologi informasi yang berorientasi kepada perkembangan ilmu pengetahuan, teknologi dan seni",
		        true);
		return view;
	}

}
