package id.adc.akakommobile.fragment;

import id.adc.akakommobile.R;
import id.adc.akakommobile.lib.TextViewEx;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class SIFragment extends Fragment {
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_si, container, false);
		
		TextViewEx samSI = (TextViewEx) view.findViewById(R.id.textSI);
		samSI.setText("Program Studi Sistem Informasi merupakan salah satu program studi yang diselenggarakan oleh STMIK AKAKOM. Program Studi Sistem Informasi mulai diselenggarakan berdasarkan SK No. 300/Dikti/Kep/1992 saat itu dengan nama Manajemen Informatika. Dan saat ini telah terakreditasi �B� berdasarkan SK No. 011/BAN-PT/Ak-X/S1/VIII/2006.\n"
				+ "Pada tahun 2009  Program Studi ini telah menerapkan kurikulum berbasis kompetensi. Dalam kurikulum yang dirancang untuk 8 semester ini, program studi mengarahkan minat sistem informasi berorientasi bisnis dan akuntansi. Dengan demikian, diharapkan lulusan akan dapat menganalisis, merancang, mengimplementasikan sistem informasi yang bisa membantu keputusan bisnis ataupun akuntansinya. Dengan kurikulum yang dirancang seperti itu, maka peminat program studi ini, bisa berasal dari berbagai jurusan yang ada di SLTA.\n"
				+ "Mahasiswa program studi ini, berasal dari berbagai daerah, yang menunjukkan bahwa program studi ini sudah dikenal di daerah mereka. Namun, tidak bisa dipungkiri bahwa program studi harus terus meningkatkan mutu agar selalu bisa bersaing.", true);
		
		return view;
	}

}
